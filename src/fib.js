// Re-set the document title
document.title = 'Fibonacci Tree';



var addSlider = function(){
    
    //This is the code to generate the regular slider section
    var div = document.createElement('div');
    div.setAttribute('class', 'blue fib-container');
    document.body.appendChild(div);
    
    var words = document.createElement('p');
    words.textContent = 'Fibonacci Slider';
    div.appendChild(words);
    
    var form = document.createElement('form');
    form.setAttribute('id', 'list-of-divs')
    div.appendChild(form);
    
    var label = document.createElement('label');
    label.setAttribute('id', 'list-label');
    label.textContent = "Fib(0)";
    form.appendChild(label);
    
    var input = document.createElement('input');
    input.setAttribute("type", "range");
    input.setAttribute("id", "list-slider");
    input.setAttribute("min", "0");
    input.setAttribute("max", "50");
    input.setAttribute("value", "0");
    input.setAttribute("oninput", "listSlider(this)");
    form.appendChild(input);
    
    var fiblist = document.createElement('div');
    fiblist.setAttribute('class' , 'fib-list');
    div.appendChild(fiblist);
    
    var fibitem = document.createElement('div');
    fibitem.setAttribute('class', 'fib-item');
    fiblist.appendChild(fibitem);
    
    
}



//addSlider();

var addTree = function(){
    
    //This is the code to generate the regular slider section
    var div = document.createElement('div');
    div.setAttribute('class', 'green fib-container');
    document.body.appendChild(div);
    
    var words = document.createElement('p');
    words.textContent = 'Fibonacci Tree';
    div.appendChild(words);
    
    var form = document.createElement('form');
    form.setAttribute('id', 'list-of-divs')
    div.appendChild(form);
    
    var label = document.createElement('label');
    label.setAttribute('id', 'tree-label');
    label.textContent = "Fib(0)";
    form.appendChild(label);
    
    var input = document.createElement('input');
    input.setAttribute("type", "range");
    input.setAttribute("id", "tree-slider");
    input.setAttribute("min", "0");
    input.setAttribute("max", "11");
    input.setAttribute("value", "0");
    input.setAttribute("oninput", "treeSlider(this)");
    form.appendChild(input);
    
    var fiblist = document.createElement('div');
    fiblist.setAttribute('class' , 'fib-list');
    div.appendChild(fiblist);
    
    var fibitem = document.createElement('div');
    fibitem.setAttribute('class', 'fib-item');
    fiblist.appendChild(fibitem);
    
    
}

addTree();



//This is where i am going to try to take input from the slider and make it display something
var listSlider = function(me) {
    
    var value = me.value;
    
    //this is adding the sliders value to the label
    
    var label1 = document.querySelector('#list-label');
    label1.textContent = `Fib(${value})`;
    
    //This is removing old fiblist
    var fibList = document.querySelector('div.fib-list');
    if (fibList) {
        fibList.remove();
    }
    
    // create a new containing <div class="fib-list">
    fibList = document.createElement('div');
    fibList.setAttribute('class', 'fib-list');
    
    
    //This is here to hard code int he 0 step/ we cant get the 1 step to work
    
    const result = [0,1];
    //now use the loop to add new divs with the fib number within.
    for (var i = 2; i <= value; i++) {
        
        var newDiv = document.createElement('div');
        var newP = document.createElement('p');
        
        const a = result[i - 1];
        const b = result[i - 2];
        result.push(a + b);
        
        newP.textContent = result[i];
        newDiv.appendChild(newP);
        newDiv.setAttribute('class', 'fib-item');
        fibList.appendChild(newDiv);
    }
    
    
    // finally, attach <div class="fib-list"> to the DOM
    var theForm = document.querySelector('#list-of-divs');
    theForm.appendChild(fibList);
    
    
}

var treeSlider = function(me) {
    //getting the value from the slider
    var form = me.parentNode; // this naming might be a problem
    
    var value = parseInt(me.value);
    
    //Put the sliders value into the label
    var label1 = document.querySelector('#tree-label');
    label1.textContent = `Fib(${value})`;
    
    //remove the existing list if there is a fib list of <divs>
    var tree = document.querySelector('#tree-of-divs');
    if (tree) {
        tree.remove();
    }
    
    tree = document.createElement('div');
    tree.id = 'tree-of-divs'
    tree.setAttribute('class', 'fib-container');
    
    var treeObj = recursiveFibTree(value);
    tree.appendChild(treeObj);
    
    form.parentNode.appendChild(tree);
}

var recursiveFibTree = function(depth) {
    
    //This is creating the fib item
    var newDiv = document.createElement('div');
    newDiv.setAttribute('class', 'fib-item');
    var newP = document.createElement('p');
    //newP.textContent = depth;
    newDiv.appendChild(newP);
    
    //we now handle the base cases of the Fib sequence
    if(depth === 0) {
        newP.textContent =  0;
        return newDiv;
    }
    else if (depth === 1) {
        newP.textContent =  1;
        return newDiv;
    }
    
    //here we recursivly find the non base case Fib numbers
    else{
        
        newP.textContent = fibonacci(depth);
        var left = recursiveFibTree(depth - 1);
        var cls = left.getAttribute('class');
        left.setAttribute('class', `fib-left ${cls}`);
        newDiv.appendChild(left);
        
        
        var right = recursiveFibTree(depth - 2);
        cls = right.getAttribute('class');
        right.setAttribute('class', `fib-right ${cls}`);
        newDiv.appendChild(right);
        
        return newDiv;
    }
    
}
//this calclated fibonacci numbers for our other class.
function fibonacci(num) {
    if (num === 0) return 0;
    
    else if (num <= 1) return 1;
    
    return fibonacci(num - 1) + fibonacci(num - 2);
}

//console.log(fibonacci(4));



